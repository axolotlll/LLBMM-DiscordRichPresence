# LLBMM-DiscordRichPresence
Discord rich presence for Lethal League Blaze. This is a mod to add details about the game to Discord activity in the status bar. Please use [LLBMM](https://github.com/MrGentle/LLB-Mod-Manager) to install.


![In Training](../main/Screenshots/richpresence_1.png?raw=True)
![In Ranked Game](../main/Screenshots/richpresence_2.png?raw=True)

![In Menus](../main/Screenshots/Screenshot_1.png?raw=True)
![Searching](../main/Screenshots/Screenshot_2.png?raw=True)




