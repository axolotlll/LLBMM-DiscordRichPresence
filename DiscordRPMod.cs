﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using Discord;
using UnityEngine;
using Debug = UnityEngine.Debug;

namespace DiscordRichPresence
{
    public class DiscordRPMod : MonoBehaviour
    {

        const string modversion = "1.0.0";
        const string repositoryOwner = "axolotl-code";
        const string repositoryName = "LLBMM-DiscordRichPresence";

        public static DiscordRPMod instance = null;

        public bool playing = false;


        public String state = "In Menus";
        public String details = "";

        private long start;

        public Dictionary<string, string> charTranslations = new Dictionary<string, string>();

        Discord.Discord discord = null;
        ActivityManager activityManager = null;

        private Discord.Activity activity = new Discord.Activity
        {
            State = "Loading...",
            Details = "",
        };
        // 

        public static void Initialize()
        {
            GameObject gameObj = new GameObject("DiscordRPMod");
            instance = gameObj.AddComponent<DiscordRPMod>();
            DontDestroyOnLoad(gameObj);

            Debug.Log("Discord rich presence initialized");

        }

        void Start()
        {
            
            discord = new Discord.Discord(930538923183202395, (UInt64) Discord.CreateFlags.Default);
            activityManager = discord.GetActivityManager();
            activityManager.RegisterSteam(553310);
            activity.Assets.LargeImage = "icon";
            //activity.Assets.SmallImage = "grid";
            charTranslations.Add("BAG", "Dust & Ashes");
            charTranslations.Add("BOOM", "Sonata");
            charTranslations.Add("BOSS", "Doombox");
            charTranslations.Add("CANDY", "Candyman");
            charTranslations.Add("CROC", "Latch");
            charTranslations.Add("ELECTRO", "Grid");
            charTranslations.Add("GRAF", "Toxic");
            charTranslations.Add("KID", "Raptor");
            charTranslations.Add("PONG", "Dice");
            charTranslations.Add("ROBOT", "Switch");
            charTranslations.Add("SKATE", "Jet");
            charTranslations.Add("50", "Cactuar");

            InvokeRepeating("DiscordUpdate", 1.0f, 1.0f);
            InvokeRepeating("DiscCallbacks", 1.0f, 1.0f);
        }

        void OnDestroy()
        {
            Debug.Log("Clearing discord status....");
            activityManager.ClearActivity((result) =>
            {
                if (result == Discord.Result.Ok)
                {
                    Debug.Log("Cleared discord status on exit");
                }
                else
                {
                    Debug.Log(result);
                }
            });
            discord.Dispose();
        }

        void DiscCallbacks()
        {
            if (discord != null)
            {
                discord.RunCallbacks();
            }
            else
            {
                Debug.Log("Discord is null!");
            }

        }

        void DiscordUpdate()
        {
            JOFJHDJHJGI gameState = DNPFJHMAIBP.HHMOGKIMBNM();
            //Debug.Log("GameState: "+gameState.ToString());
            JOMBNFKIHIC game = JOMBNFKIHIC.GIGAKBJGFDI;

           
            //todo try HDLIJDFGKN.instance.HCCGAFLIABM
            // this is ranked character
            //Debug.Log(ALDOKEMAOMB.BJDPHEHJJJK(HDLIJDBFGKN.instance.HCCGAFLIABM).DOFCCEDJODB.ToString());
            String icon = "icon";
            String largeText = "";
            //Debug.Log("OOE: "+game.OOEPDFABFIP);
            String chara = "";
            if (game.OOEPDFABFIP != 0)
            {
                
                icon = game.OOEPDFABFIP.ToString();
                largeText = "Map: " + icon;
            }
            //String stage = game.OOEPDFABFIP.ToString();
            //Debug.Log(stage);
            bool _playing = false;

            if (gameState == JOFJHDJHJGI.NBNDONMABLK)
            {
                if (JOMBNFKIHIC.EAENFOJNNGP == OnlineMode.RANKED)
                {
                    state = "Searching for Ranked Game";
                }


                else if (JOMBNFKIHIC.EAENFOJNNGP == OnlineMode.HOSTED)
                {
                    state = "In Private Lobby";
                }

                else if (JOMBNFKIHIC.EAENFOJNNGP == OnlineMode.QUICKMATCH)
                {
                    state = "Searching for Quick Match";
                }

                
            }
            else if (gameState == JOFJHDJHJGI.CDOFDJMLGLO)
            {
                if (JOMBNFKIHIC.EAENFOJNNGP == OnlineMode.RANKED)
                {
                    state = "In Ranked Match";
                    chara = ALDOKEMAOMB.BJDPHEHJJJK(HDLIJDBFGKN.instance.HCCGAFLIABM).DOFCCEDJODB.ToString();
                }


                else if (JOMBNFKIHIC.EAENFOJNNGP == OnlineMode.HOSTED)
                {
                    state = "In Private Match";
                    chara = ALDOKEMAOMB.BJDPHEHJJJK(HDLIJDBFGKN.instance.HCCGAFLIABM).DOFCCEDJODB.ToString();
                }

                else if (JOMBNFKIHIC.EAENFOJNNGP == OnlineMode.QUICKMATCH)
                {
                    state = "In Quick Match";
                    chara = ALDOKEMAOMB.BJDPHEHJJJK(HDLIJDBFGKN.instance.HCCGAFLIABM).DOFCCEDJODB.ToString();
                }
                else
                {
                    if (ALDOKEMAOMB.OBIEGJGBIDO > 1)
                    {
                        if (ALDOKEMAOMB.BJDPHEHJJJK(1).DOFCCEDJODB == Character.NONE)
                        {
                            state = "In Training Mode";
                        }
                        else
                        {
                            state = "In Local Game";
                        }
                    }
                    else
                    {
                        state = "In Training Mode";
                    }

                    chara = ALDOKEMAOMB.BJDPHEHJJJK(0).DOFCCEDJODB.ToString();
                }

                _playing = true;
            }
            else if (gameState == JOFJHDJHJGI.FADIBJAINDE)
            {
                state = "In Local Game";
                _playing = true;
            }
            else if (gameState == JOFJHDJHJGI.LGILIJKMKOD) // game pause
            {
                //Debug.Log("Esc out, passing");
                _playing = true;
            }
            
            else
            {
                state = "In Menus";
            }

            if (!playing && _playing)
            {
                start = (long)(DateTime.UtcNow - new DateTime(1970, 1, 1, 0, 0, 0, 0)).TotalSeconds;
                playing = true;
            }

            if (!_playing)
            {
                playing = false;
            }

            if (playing)
            {
                //TimeSpan ts = stopwatch.Elapsed;
                //details = "Playing on " + stage +"\n";
                //details = details + "Time Elapsed: " + String.Format("{0:00}:{1:00}:{2:00}", ts.Hours, ts.Minutes, ts.Seconds);

                //details = "";
                activity.Timestamps = new ActivityTimestamps()
                {
                    Start = start
                };
                activity.Assets.LargeImage = icon.ToLower();
                activity.Assets.LargeText = largeText;
                
            }
            else
            {
                details = "";
                activity.Timestamps = new ActivityTimestamps();
                activity.Assets.LargeImage = "icon";
                activity.Assets.LargeText = "Lethal League Blaze";
            }

            //Debug.Log(chara);
            if (chara.Length > 0 && playing)
            {
                activity.Assets.SmallImage = chara.ToLower();
                String charout = "Null";
                charTranslations.TryGetValue(chara, out charout);
                activity.Assets.SmallText = "Playing " + charout;
            }
            else
            {
                activity.Assets.SmallImage = "";
                activity.Assets.SmallText = "";
            }

            activity.State = details;
            activity.Details = state; // goof 
            
            
            activityManager.UpdateActivity(activity, (res) =>
            {
                if (res != Discord.Result.Ok)
                {
                    Debug.Log("Error updating discord... " + res.ToString());
                }
            });
        }
    }
}
